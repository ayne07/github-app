package ccsantiago.com.githubapp.volley.response;

import java.util.ArrayList;

import ccsantiago.com.githubapp.models.Email;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class GetEmailsResponse {

    public ArrayList<Email> emails;

}
