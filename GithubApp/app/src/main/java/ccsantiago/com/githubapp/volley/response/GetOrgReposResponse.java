package ccsantiago.com.githubapp.volley.response;

import java.util.ArrayList;

import ccsantiago.com.githubapp.models.Repository;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class GetOrgReposResponse {
    public ArrayList<Repository> repositories;
}
