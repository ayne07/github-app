package ccsantiago.com.githubapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.models.Repository;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class ReposAdapter extends ArrayAdapter<Repository> {


    private Context context;
    private ArrayList<Repository> repositories;


    public ReposAdapter(Context context, ArrayList<Repository> repositories) {
        super(context, R.layout.listview_item_repo, repositories);
        this.context = context;
        this.repositories = repositories;
    }


    private class ViewHolder {
        TextView nameTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_item_repo, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameTextView = (TextView) view.findViewById(R.id.textViewName);
            view.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.nameTextView.setText(repositories.get(position).getName());
        return view;
    }
}
