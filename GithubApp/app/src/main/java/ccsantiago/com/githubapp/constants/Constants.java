package ccsantiago.com.githubapp.constants;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class Constants {

    public static final String GITHUB_BASE_URL = "https://api.github.com";
    public static final int OK_STATUS_CODE = 200;
    public static final int NOT_MODIFIED_CODE = 304;
    public static final String USERNAME_KEY = "username_key";
    public static final String PASSWORD_KEY = "password_key";
    public static final String EMAIL_KEY = "email_key";
    public static final String ALL_EMAILS_KEY = "all_emails_key";
    public static final String ORGS_KEY = "orgs_key";
    public static final String PERSONAL_REPOS_KEY = "personal_repos_key";
    public static final String ORG_REPOS_KEY = "org_repos_key";
    public static final String REPOS_URL_KEY = "repos_url_key";
    public static final String NAME_KEY = "name_key";
    public static final String COMPANY_KEY = "company_key";
    public static final String ORG_NAME_KEY = "org_name_key";
    public static final String IS_LOGGED_IN_KEY = "is_logged_in_key";

}
