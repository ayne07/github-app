package ccsantiago.com.githubapp.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.fragments.OrganizationsFragment;
import ccsantiago.com.githubapp.fragments.PersonalReposFragment;
import ccsantiago.com.githubapp.fragments.UserAccountFragment;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class TabHostFragmentActivity extends FragmentActivity {

    private FragmentTabHost fragmentTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmenttab);

        fragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        fragmentTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        fragmentTabHost.addTab(
                fragmentTabHost.newTabSpec("Profile").setIndicator("Profile", null),
                UserAccountFragment.class, null);
        fragmentTabHost.addTab(
                fragmentTabHost.newTabSpec("Personal Repos").setIndicator("Personal Repos", null),
                PersonalReposFragment.class, null);
        fragmentTabHost.addTab(
                fragmentTabHost.newTabSpec("Organizations").setIndicator("Organizations", null),
                OrganizationsFragment.class, null);

    }

}
