package ccsantiago.com.githubapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.VolleyHelper;
import ccsantiago.com.githubapp.volley.request.AuthenticateRequest;
import ccsantiago.com.githubapp.volley.response.AuthenticateResponse;


public class MainActivity extends ActionBarActivity implements View.OnClickListener,
        Response.Listener, Response.ErrorListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private EditText usernameEditText;
    private EditText passwordEditText;
    private ProgressDialog progressDialog;
    private SharedPreferencesHelper sharedPreferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferencesHelper = SharedPreferencesHelper.getInstance(this);

        if (sharedPreferencesHelper.isLoggedIn()) {
            Intent intent = new Intent(this, TabHostFragmentActivity.class);
            intent.putExtra(Constants.USERNAME_KEY, sharedPreferencesHelper.getUsername());
            intent.putExtra(Constants.PASSWORD_KEY, sharedPreferencesHelper.getPassword());
            startActivity(intent);
            finish();
        }


        findViewById(R.id.button_login).setOnClickListener(this);
        usernameEditText = (EditText) findViewById(R.id.edittext_username);
        passwordEditText = (EditText) findViewById(R.id.edittext_password);


    }

    @Override
    protected void onResume(){
        super.onResume();
        if(sharedPreferencesHelper.isLoggedIn()){
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button_login) {

            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            if (!TextUtils.isEmpty(username) &&
                    !TextUtils.isEmpty(password)) {
                doAuthenticateRequest(username, password);
            }


        }

    }

    private void doAuthenticateRequest(String username, String password) {
        progressDialog = ProgressDialog.show(this, "",
                getString(R.string.authenticating));
        VolleyHelper.getInstance(this).addToRequestQueue(new AuthenticateRequest(
                username, password, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (error.networkResponse != null) {
            String errorString = new String(error.networkResponse.data);
            Toast.makeText(this, "Unable to login! " + errorString, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Uable to login! Please check your connection settings",
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (response instanceof AuthenticateResponse) {
            AuthenticateResponse authenticateResponse = (AuthenticateResponse) response;
            Log.d(TAG, "statusCode: " + authenticateResponse.statusCode + " username " +
                    authenticateResponse.username);
            if (authenticateResponse.statusCode == Constants.OK_STATUS_CODE
                    || authenticateResponse.statusCode == Constants.NOT_MODIFIED_CODE) {
                sharedPreferencesHelper.setIsLoggedIn(true);
                sharedPreferencesHelper.setUsername(authenticateResponse.username);
                sharedPreferencesHelper.setPassword(authenticateResponse.password);
                Intent intent = new Intent(this, TabHostFragmentActivity.class);
                intent.putExtra(Constants.USERNAME_KEY, authenticateResponse.username);
                intent.putExtra(Constants.PASSWORD_KEY, authenticateResponse.password);
                startActivity(intent);
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }


}
