package ccsantiago.com.githubapp.volley.response;

import java.util.ArrayList;

import ccsantiago.com.githubapp.models.Organization;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class GetOrganizationResponse {

    public String username;
    public String password;
    public ArrayList<Organization> organizations;
}
