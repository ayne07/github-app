package ccsantiago.com.githubapp.volley.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.volley.response.GetUserInfoResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class GetUserInfoRequest extends BaseRequest<GetUserInfoResponse> {

    private static final String TAG = GetUserInfoRequest.class.getSimpleName();

    private Response.Listener<GetUserInfoResponse> responseListener;
    private Response.ErrorListener errorListener;
    private String username;
    private String password;


    public GetUserInfoRequest(String username, String password, Response.Listener responseListener,
                               Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, Constants.GITHUB_BASE_URL+"/user", responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.username = username;
        this.password = password;
    }

    @Override
    protected Response<GetUserInfoResponse> parseNetworkResponse(NetworkResponse response) {
        GetUserInfoResponse getUserInfoResponse = new GetUserInfoResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);

            JSONObject jsonObject = new JSONObject(responseString);
            String name = jsonObject.getString("name");
            String company = jsonObject.getString("company");
            String publicEmail = jsonObject.getString("email");
            getUserInfoResponse.name = name;
            getUserInfoResponse.company = company;
            getUserInfoResponse.publicEmail = publicEmail;

            return Response.success(getUserInfoResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void deliverResponse(GetUserInfoResponse response) {
        if (responseListener != null) {
            responseListener.onResponse(response);
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        if (errorListener != null) {
            errorListener.onErrorResponse(error);
        }
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }
}
