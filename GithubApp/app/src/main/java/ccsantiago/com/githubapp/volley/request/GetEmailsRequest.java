package ccsantiago.com.githubapp.volley.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.response.GetEmailsResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class GetEmailsRequest extends BaseRequest<GetEmailsResponse> {
    private static final String TAG = GetEmailsRequest.class.getSimpleName();

    private Response.Listener<GetEmailsRequest> responseListener;
    private Response.ErrorListener errorListener;
    private Context context;

    public GetEmailsRequest(Context context, String username, String password,
                            Response.Listener responseListener,
                            Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, Constants.GITHUB_BASE_URL + "/user/emails",
                responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.context = context;
    }

    @Override
    protected Response<GetEmailsResponse> parseNetworkResponse(NetworkResponse response) {
        GetEmailsResponse getEmailsResponse = new GetEmailsResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);

            JSONArray jsonArray = new JSONArray(responseString);

            SharedPreferencesHelper.getInstance(context).setAllEmails(jsonArray);
            getEmailsResponse.emails = SharedPreferencesHelper.getInstance(context).getAllEmails();

            return Response.success(getEmailsResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
