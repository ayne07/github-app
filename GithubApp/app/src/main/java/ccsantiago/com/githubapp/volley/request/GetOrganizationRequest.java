package ccsantiago.com.githubapp.volley.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.response.GetOrganizationResponse;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class GetOrganizationRequest extends BaseRequest<GetOrganizationResponse> {

    private static final String TAG = GetOrganizationRequest.class.getSimpleName();

    private Response.Listener<GetEmailsRequest> responseListener;
    private Response.ErrorListener errorListener;
    private String username;
    private String password;
    private Context context;

    public GetOrganizationRequest(Context context, String username, String password,
                                  Response.Listener responseListener,
                                  Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, Constants.GITHUB_BASE_URL + "/user/orgs",
                responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.username = username;
        this.password = password;
        this.context = context;

    }

    @Override
    protected Response<GetOrganizationResponse> parseNetworkResponse(NetworkResponse response) {

        GetOrganizationResponse getOrganizationResponse = new GetOrganizationResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);
            getOrganizationResponse.username = username;
            getOrganizationResponse.password = password;
            JSONArray jsonArray = new JSONArray(responseString);
            SharedPreferencesHelper.getInstance(context).setOrganizations(jsonArray);
            getOrganizationResponse.organizations = SharedPreferencesHelper.getInstance(context)
                    .getOrganizations();

            return Response.success(getOrganizationResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
