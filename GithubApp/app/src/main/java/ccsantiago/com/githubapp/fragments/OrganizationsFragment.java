package ccsantiago.com.githubapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.activities.OrgReposFragmentActivity;
import ccsantiago.com.githubapp.adapters.OrganizationsAdapter;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.models.Organization;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.VolleyHelper;
import ccsantiago.com.githubapp.volley.request.GetOrganizationRequest;
import ccsantiago.com.githubapp.volley.response.GetOrganizationResponse;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class OrganizationsFragment extends Fragment implements Response.Listener,
        Response.ErrorListener, AdapterView.OnItemClickListener {

    private Activity activity;

    private ListView listView;

    private ProgressDialog progressDialog;
    private String username;
    private String password;

    public OrganizationsFragment() {

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        username = activity.getIntent().getStringExtra(Constants.USERNAME_KEY);
        password = activity.getIntent().getStringExtra(Constants.PASSWORD_KEY);

        View view = inflater.inflate(R.layout.fragment_organizations, container, false);
        listView = (ListView) view.findViewById(R.id.listViewOrganizations);
        listView.setAdapter(new OrganizationsAdapter(activity,
                SharedPreferencesHelper.getInstance(activity).getOrganizations()));
        listView.setOnItemClickListener(this);


        doGetOrganizationRequest();


        return view;

    }

    private void doGetOrganizationRequest() {
        progressDialog = ProgressDialog.show(activity, "",
                getString(R.string.loading_orgs));
        VolleyHelper.getInstance(activity).addToRequestQueue(new GetOrganizationRequest(activity,
                username,
                password,
                this, this));
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (response instanceof GetOrganizationResponse) {
            GetOrganizationResponse getOrganizationResponse = (GetOrganizationResponse) response;
            listView.setAdapter(new OrganizationsAdapter(activity,
                    getOrganizationResponse.organizations));
            listView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Organization organization = (Organization) parent.getAdapter().getItem(position);
        Intent intent = new Intent(activity, OrgReposFragmentActivity.class);
        intent.putExtra(Constants.REPOS_URL_KEY, organization.getReposUrl());
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }
}
