package ccsantiago.com.githubapp.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.adapters.ReposAdapter;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.VolleyHelper;
import ccsantiago.com.githubapp.volley.request.GetOrgReposRequest;
import ccsantiago.com.githubapp.volley.response.GetOrgReposResponse;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class OrgReposFragmentActivity extends FragmentActivity implements Response.Listener,
        Response.ErrorListener {


    private String repos_url;
    private String org_name;
    private String username;
    private String password;
    private ListView listView;
    private int page = 1;
    private int preLastIndex = 0;
    private ProgressDialog progressDialog;
    private ReposAdapter adapter;
    private SharedPreferencesHelper sharedPreferencesHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_personalrepos);

        sharedPreferencesHelper = SharedPreferencesHelper.getInstance(this);

        username = sharedPreferencesHelper.getUsername();
        password = sharedPreferencesHelper.getPassword();
        repos_url = getIntent().getStringExtra(Constants.REPOS_URL_KEY);
        org_name = getIntent().getStringExtra(Constants.ORG_NAME_KEY);

        listView = (ListView) findViewById(R.id.listViewRepos);

        adapter = new ReposAdapter(this, sharedPreferencesHelper.getOrgRepos(org_name));
        listView.setAdapter(adapter);


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (preLastIndex != lastItem) {
                        Log.d("List", "End of list reached");
                        preLastIndex = lastItem;
                        page++;
                        doGetOrgReposRequest();
                    }
                }
            }
        });

        doGetOrgReposRequest();

    }

    private void doGetOrgReposRequest() {
        progressDialog = ProgressDialog.show(this, "",
                getString(R.string.loading_repos));
        VolleyHelper.getInstance(this).addToRequestQueue(new GetOrgReposRequest
                (this, username, password, org_name,
                        repos_url, page, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onResponse(Object response) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if (response instanceof GetOrgReposResponse) {
            GetOrgReposResponse getOrgReposResponse = (GetOrgReposResponse) response;
            preLastIndex = 0;
            adapter.addAll(getOrgReposResponse.repositories);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        progressDialog = null;
    }
}
