package ccsantiago.com.githubapp.volley.request;

import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public abstract class BaseRequest<T> extends Request<T> {

    private final Response.Listener listener;
    private final Response.ErrorListener errorListener;
    private String username;
    private String password;

    public BaseRequest(String username, String password, int method,
                       String url, Response.Listener<T> listener,
                       Response.ErrorListener
                               errorListener) {
        super(method, url, errorListener);
        this.listener = listener;
        this.errorListener = errorListener;
        this.username = username;
        this.password = password;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Authorization", "Basic " + Base64.encodeToString(
                (username + ":" + password).getBytes(),
                Base64.NO_WRAP));
        return headerMap;
    }

    @Override
    public Request.Priority getPriority() {
        return Priority.NORMAL;
    }

    @Override
    public void deliverError(VolleyError error) {
        if (errorListener != null) {
            errorListener.onErrorResponse(error);
        }
    }
}
