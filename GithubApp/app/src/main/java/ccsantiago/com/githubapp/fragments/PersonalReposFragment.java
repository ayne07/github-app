package ccsantiago.com.githubapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.adapters.ReposAdapter;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.VolleyHelper;
import ccsantiago.com.githubapp.volley.request.GetReposRequest;
import ccsantiago.com.githubapp.volley.response.GetReposResponse;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class PersonalReposFragment extends Fragment implements Response.Listener,
        Response.ErrorListener {

    private Activity activity;
    private ListView listView;
    private ProgressDialog progressDialog;
    private String username;
    private String password;

    public PersonalReposFragment() {

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        username = activity.getIntent().getStringExtra(Constants.USERNAME_KEY);
        password = activity.getIntent().getStringExtra(Constants.PASSWORD_KEY);

        View view = inflater.inflate(R.layout.fragment_personalrepos, container, false);

        listView = (ListView) view.findViewById(R.id.listViewRepos);
        listView.setAdapter(new ReposAdapter(activity, SharedPreferencesHelper.getInstance
                (activity).getPersonalRepos()));


        doGetReposRequest();
        return view;

    }

    private void doGetReposRequest() {
        progressDialog = ProgressDialog.show(activity, "",
                getString(R.string.loading_repos));
        VolleyHelper.getInstance(activity).addToRequestQueue(new GetReposRequest(activity,
                username, password,
                this, this));
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (response instanceof GetReposResponse) {
            GetReposResponse getReposResponse = (GetReposResponse) response;
            listView.setAdapter(new ReposAdapter(activity, getReposResponse.repositories));
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }
}
