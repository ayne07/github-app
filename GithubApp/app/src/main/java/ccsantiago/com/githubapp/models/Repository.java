package ccsantiago.com.githubapp.models;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class Repository {

    private String name;
    private String url;

    public Repository(String name, String url) {

        this.name = name;
        this.url = url;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }
}
