package ccsantiago.com.githubapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.adapters.EmailsAdapter;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.VolleyHelper;
import ccsantiago.com.githubapp.volley.request.GetEmailsRequest;
import ccsantiago.com.githubapp.volley.request.GetUserInfoRequest;
import ccsantiago.com.githubapp.volley.response.GetEmailsResponse;
import ccsantiago.com.githubapp.volley.response.GetUserInfoResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class UserAccountFragment extends Fragment implements Response.Listener,
        Response.ErrorListener, View.OnClickListener {

    private static final String TAG = UserAccountFragment.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextView textViewName;
    private TextView textViewCompany;
    private TextView textViewEmail;
    private Activity activity;
    private ListView listViewEmails;
    private SharedPreferencesHelper sharedPreferencesHelper;
    private String username;
    private String password;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        sharedPreferencesHelper = SharedPreferencesHelper.getInstance(activity);
        username = activity.getIntent().getStringExtra(Constants.USERNAME_KEY);
        password = activity.getIntent().getStringExtra(Constants.PASSWORD_KEY);



        View view = inflater.inflate(R.layout.fragment_useraccount, container, false);
        textViewCompany = (TextView) view.findViewById(R.id.textViewCompany);
        textViewName = (TextView) view.findViewById(R.id.textViewName);
        textViewEmail = (TextView) view.findViewById(R.id.textViewEmail);
        listViewEmails = (ListView) view.findViewById(R.id.listViewEmails);
        listViewEmails.setAdapter(new EmailsAdapter(activity,
                sharedPreferencesHelper.getAllEmails()));

        view.findViewById(R.id.buttonLogout).setOnClickListener(this);

        textViewName.setText("Name: " + sharedPreferencesHelper.getName());
        textViewCompany.setText("Company: " + sharedPreferencesHelper.getCompany());
        textViewEmail.setText("Public Email: " + sharedPreferencesHelper.getEmail());


        doGetUserInfoRequest();

        return view;

    }

    private void doGetUserInfoRequest() {
        progressDialog = ProgressDialog.show(activity, "",
                getString(R.string.loading_user_info));
        VolleyHelper.getInstance(activity).addToRequestQueue(new GetUserInfoRequest(username,
                password,
                this, this));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void onResponse(Object response) {


        if (response instanceof GetUserInfoResponse) {
            GetUserInfoResponse getUserInfoResponse = (GetUserInfoResponse) response;
            textViewName.setText("Name: " + getUserInfoResponse.name);
            textViewCompany.setText("Company: " + getUserInfoResponse.company);
            textViewEmail.setText("Public Email: " + getUserInfoResponse.publicEmail);
            sharedPreferencesHelper.setEmail(getUserInfoResponse.publicEmail);
            sharedPreferencesHelper.setName(getUserInfoResponse.name);
            sharedPreferencesHelper.setCompany(getUserInfoResponse.company);
            VolleyHelper.getInstance(activity).addToRequestQueue(new GetEmailsRequest(activity,
                    username,
                    password,
                    this, this));
        }

        if (response instanceof GetEmailsResponse) {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            GetEmailsResponse emailsResponse = (GetEmailsResponse) response;
            listViewEmails.setAdapter(new EmailsAdapter(activity, emailsResponse.emails));
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonLogout){
            sharedPreferencesHelper.clear();
            sharedPreferencesHelper.setIsLoggedIn(false);
            activity.finish();
        }
    }
}
