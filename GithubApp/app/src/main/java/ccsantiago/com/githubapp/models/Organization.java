package ccsantiago.com.githubapp.models;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class Organization {

    private String login;
    private String url;
    private String repos_url;

    public Organization(String login, String url, String repos_url){
        this.login = login;
        this.url = url;
        this.repos_url = repos_url;
    }

    public String getLogin(){
        return this.login;
    }

    public String getReposUrl(){
        return this.repos_url;
    }

    public String getUrl(){
        return this.url;
    }
}
