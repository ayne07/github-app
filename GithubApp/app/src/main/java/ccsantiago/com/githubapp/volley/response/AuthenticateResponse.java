package ccsantiago.com.githubapp.volley.response;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class AuthenticateResponse {
    public int statusCode;
    public String username;
    public String password;

}
