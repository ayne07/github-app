package ccsantiago.com.githubapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.models.Email;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class EmailsAdapter extends ArrayAdapter<Email> {


    private Context context;
    private ArrayList<Email> emails;


    public EmailsAdapter(Context context, ArrayList<Email> emails) {
        super(context, R.layout.listview_item_email, emails);
        this.context = context;
        this.emails = emails;
    }


    private class ViewHolder {
        TextView emailTextView;
        TextView primaryTextView;
        TextView verifiedTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_item_email, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.emailTextView = (TextView) view.findViewById(R.id.textViewEmail);
            viewHolder.primaryTextView = (TextView) view.findViewById(R.id.textViewPrimary);
            viewHolder.verifiedTextView = (TextView) view.findViewById(R.id.textViewVerified);
            view.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.emailTextView.setText(emails.get(position).getEmail());
        viewHolder.primaryTextView.setText(emails.get(position).getPrimary() ? "(primary)" : "(not" +
                " primary)");
        viewHolder.verifiedTextView.setText(emails.get(position).getVerified() ? "(verified)" :
                "(not verified");
        return view;
    }
}
