package ccsantiago.com.githubapp.volley.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.response.GetReposResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class GetReposRequest extends BaseRequest<GetReposResponse> {
    private static final String TAG = GetReposRequest.class.getSimpleName();

    private Response.Listener<GetReposResponse> responseListener;
    private Response.ErrorListener errorListener;
    private Context context;

    public GetReposRequest(Context context, String username, String password,
                           Response.Listener responseListener,
                           Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, Constants.GITHUB_BASE_URL + "/user/repos",
                responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.context = context;
    }

    @Override
    protected Response<GetReposResponse> parseNetworkResponse(NetworkResponse response) {
        GetReposResponse getReposResponse = new GetReposResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);


            JSONArray jsonArray = new JSONArray(responseString);
            SharedPreferencesHelper.getInstance(context).setPersonalRepos(jsonArray);
            getReposResponse.repositories = SharedPreferencesHelper.getInstance(context)
                    .getPersonalRepos();

            return Response.success(getReposResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
