package ccsantiago.com.githubapp.volley.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import ccsantiago.com.githubapp.singletons.SharedPreferencesHelper;
import ccsantiago.com.githubapp.volley.response.GetOrgReposResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class GetOrgReposRequest extends BaseRequest<GetOrgReposResponse> {
    private static final String TAG = GetOrgReposRequest.class.getSimpleName();

    private Response.Listener<GetOrgReposResponse> responseListener;
    private Response.ErrorListener errorListener;
    private Context context;
    private String orgName;
    private int page;

    public GetOrgReposRequest(Context context, String username, String password, String orgName,
                              String orgReposUrl, int page,
                              Response.Listener responseListener,
                              Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, orgReposUrl,
                responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.context = context;
        this.orgName = orgName;
        this.page = page;
    }

    @Override
    protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", "" + page);
        return params;
    }

    @Override
    protected Response<GetOrgReposResponse> parseNetworkResponse(NetworkResponse response) {
        GetOrgReposResponse getOrgReposResponse = new GetOrgReposResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);


            JSONArray jsonArray = new JSONArray(responseString);
            SharedPreferencesHelper.getInstance(context).setOrgRepos(orgName, jsonArray);
            getOrgReposResponse.repositories = SharedPreferencesHelper.getInstance(context)
                    .getOrgRepos(orgName);
            return Response.success(getOrgReposResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
