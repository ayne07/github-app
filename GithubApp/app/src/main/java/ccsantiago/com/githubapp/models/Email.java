package ccsantiago.com.githubapp.models;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class Email {

    private String email;
    private boolean primary;
    private boolean verified;


    public Email(){
        super();
    }

    public Email(String email, boolean primary, boolean verified){
        this.email = email;
        this.primary = primary;
        this.verified = verified;

    }

    public String getEmail(){
        return this.email;
    }

    public boolean getVerified(){
       return this.verified;
    }

    public boolean getPrimary(){
        return this.primary;
    }

}
