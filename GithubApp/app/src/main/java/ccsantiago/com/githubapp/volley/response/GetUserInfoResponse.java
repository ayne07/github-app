package ccsantiago.com.githubapp.volley.response;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class GetUserInfoResponse {

    public String name;
    public String company;
    public String publicEmail;
}
