package ccsantiago.com.githubapp.volley.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;

import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.volley.response.AuthenticateResponse;

/**
 * Created by charmanesantiago on 3/10/15.
 */
public class AuthenticateRequest extends BaseRequest<AuthenticateResponse> {

    private static final String TAG = AuthenticateRequest.class.getSimpleName();

    private Response.Listener<AuthenticateResponse> responseListener;
    private Response.ErrorListener errorListener;
    private String username;
    private String password;


    public AuthenticateRequest(String username, String password, Response.Listener responseListener,
                               Response.ErrorListener errorListener) {
        super(username, password, Request.Method.GET, Constants.GITHUB_BASE_URL,
                responseListener, errorListener);
        this.responseListener = responseListener;
        this.errorListener = errorListener;
        this.username = username;
        this.password = password;
    }

    @Override
    protected Response<AuthenticateResponse> parseNetworkResponse(NetworkResponse response) {
        AuthenticateResponse authenticateResponse = new AuthenticateResponse();
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response
                    .headers));
            Log.d(TAG, "response: " + responseString);
            authenticateResponse.statusCode = response.statusCode;
            authenticateResponse.username = username;
            authenticateResponse.password = password;
            return Response.success(authenticateResponse, HttpHeaderParser.parseCacheHeaders
                    (response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void deliverResponse(AuthenticateResponse response) {
        if (responseListener != null) {
            responseListener.onResponse(response);
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        if (errorListener != null) {
            errorListener.onErrorResponse(error);
        }
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }
}
