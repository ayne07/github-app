package ccsantiago.com.githubapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.models.Organization;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class OrganizationsAdapter extends ArrayAdapter<Organization> {


    private Context context;
    private ArrayList<Organization> organizations;


    public OrganizationsAdapter(Context context, ArrayList<Organization> organizations) {
        super(context, R.layout.listview_item_organization, organizations);
        this.context = context;
        this.organizations = organizations;
    }


    private class ViewHolder {
        TextView orgTextView;
        TextView reposUrlTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_item_organization, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.orgTextView = (TextView) view.findViewById(R.id.textViewOrg);
            viewHolder.reposUrlTextView = (TextView) view.findViewById(R.id.textViewReposUrl);
            view.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.orgTextView.setText(organizations.get(position).getLogin());
        viewHolder.reposUrlTextView.setText(organizations.get(position).getReposUrl());
        return view;
    }
}
