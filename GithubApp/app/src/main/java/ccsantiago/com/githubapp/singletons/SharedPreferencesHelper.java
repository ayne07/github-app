package ccsantiago.com.githubapp.singletons;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ccsantiago.com.githubapp.R;
import ccsantiago.com.githubapp.constants.Constants;
import ccsantiago.com.githubapp.models.Email;
import ccsantiago.com.githubapp.models.Organization;
import ccsantiago.com.githubapp.models.Repository;

/**
 * Created by charmanesantiago on 3/11/15.
 */
public class SharedPreferencesHelper {

    private static SharedPreferencesHelper sInstance;
    private SharedPreferences sSharedPreferences;

    private SharedPreferencesHelper(Context context) {
        sSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name),
                Context.MODE_PRIVATE);
    }

    public static SharedPreferencesHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferencesHelper(context);
        }

        return sInstance;
    }

    public void setIsLoggedIn(boolean isLoggedIn){
        sSharedPreferences.edit().putBoolean(Constants.IS_LOGGED_IN_KEY, isLoggedIn).commit();
    }

    public boolean isLoggedIn(){
        return sSharedPreferences.getBoolean(Constants.IS_LOGGED_IN_KEY,false);
    }

    public void setUsername(String username) {
        sSharedPreferences.edit().putString(Constants.USERNAME_KEY, username).commit();
    }

    public String getUsername() {
        return sSharedPreferences.getString(Constants.USERNAME_KEY, null);
    }


    public void setPassword(String password) {
        sSharedPreferences.edit().putString(Constants.PASSWORD_KEY, password).commit();
    }

    public String getPassword() {
        return sSharedPreferences.getString(Constants.PASSWORD_KEY, null);
    }


    public void setName(String name) {
        sSharedPreferences.edit().putString(Constants.NAME_KEY, name).commit();
    }

    public String getName() {
        return sSharedPreferences.getString(Constants.NAME_KEY, null);
    }

    public void setCompany(String company) {
        sSharedPreferences.edit().putString(Constants.COMPANY_KEY, company).commit();
    }

    public String getCompany() {
        return sSharedPreferences.getString(Constants.COMPANY_KEY, null);
    }


    public void setEmail(String email) {
        sSharedPreferences.edit().putString(Constants.EMAIL_KEY, email).commit();
    }


    public String getEmail() {
        return sSharedPreferences.getString(Constants.EMAIL_KEY, null);
    }

    public void setAllEmails(JSONArray jsonArray) {
        sSharedPreferences.edit().putString(Constants.ALL_EMAILS_KEY,
                jsonArray.toString()).commit();
    }

    public ArrayList<Email> getAllEmails() {
        List<Email> emails;
        String jsonEmails = sSharedPreferences.getString(Constants.ALL_EMAILS_KEY, null);

        if (TextUtils.isEmpty(jsonEmails)) {
            return new ArrayList<Email>();
        }

        Gson gson = new Gson();
        Email[] emailItems = gson.fromJson(jsonEmails,
                Email[].class);

        emails = Arrays.asList(emailItems);
        emails = new ArrayList<Email>(emails);

        return (ArrayList<Email>) emails;
    }

    public void setOrganizations(JSONArray jsonArray) {
        sSharedPreferences.edit().putString(Constants.ORGS_KEY,
                jsonArray.toString()).commit();
    }

    public ArrayList<Organization> getOrganizations() {
        List<Organization> organizations;
        String jsonOrgs = sSharedPreferences.getString(Constants.ORGS_KEY, null);

        if (TextUtils.isEmpty(jsonOrgs)) {
            return new ArrayList<Organization>();
        }

        Gson gson = new Gson();
        Organization[] organizationItems = gson.fromJson(jsonOrgs,
                Organization[].class);

        organizations = Arrays.asList(organizationItems);
        organizations = new ArrayList<Organization>(organizations);

        return (ArrayList<Organization>) organizations;
    }

    public void setPersonalRepos(JSONArray jsonArray) {
        sSharedPreferences.edit().putString(Constants.PERSONAL_REPOS_KEY,
                jsonArray.toString()).commit();
    }

    public ArrayList<Repository> getPersonalRepos() {
        List<Repository> repositories;
        String jsonRepos = sSharedPreferences.getString(Constants.PERSONAL_REPOS_KEY, null);

        if (TextUtils.isEmpty(jsonRepos)) {
            return new ArrayList<Repository>();
        }

        Gson gson = new Gson();
        Repository[] repositoryItems = gson.fromJson(jsonRepos,
                Repository[].class);

        repositories = Arrays.asList(repositoryItems);
        repositories = new ArrayList<Repository>(repositories);

        return (ArrayList<Repository>) repositories;
    }

    public void setOrgRepos(String orgName, JSONArray jsonArray) {
        sSharedPreferences.edit().putString(orgName,
                jsonArray.toString()).commit();
    }

    public ArrayList<Repository> getOrgRepos(String orgName) {
        List<Repository> repositories;
        String jsonRepos = sSharedPreferences.getString(orgName, null);

        if (TextUtils.isEmpty(jsonRepos)) {
            return new ArrayList<Repository>();
        }

        Gson gson = new Gson();
        Repository[] repositoryItems = gson.fromJson(jsonRepos,
                Repository[].class);

        repositories = Arrays.asList(repositoryItems);
        repositories = new ArrayList<Repository>(repositories);

        return (ArrayList<Repository>) repositories;
    }

    public ArrayList<Repository> jsonArrayToRepoArray(JSONArray jsonArray){
        List<Repository> repositories;
        String jsonRepos = jsonArray.toString();

        if (TextUtils.isEmpty(jsonRepos)) {
            return new ArrayList<Repository>();
        }

        Gson gson = new Gson();
        Repository[] repositoryItems = gson.fromJson(jsonRepos,
                Repository[].class);

        repositories = Arrays.asList(repositoryItems);
        repositories = new ArrayList<Repository>(repositories);

        return (ArrayList<Repository>) repositories;
    }

    public void clear(){
        sSharedPreferences.edit().clear().commit();
    }

}
