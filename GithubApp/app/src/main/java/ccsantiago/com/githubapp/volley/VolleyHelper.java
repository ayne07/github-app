package ccsantiago.com.githubapp.volley;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by charmanesantiago on 3/10/15.
 */

public class VolleyHelper {

    private RequestQueue mRequestQueue;
    private static VolleyHelper sInstance;
    private Request mCurrentRequest;

    public static VolleyHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VolleyHelper(context);
        }
        return sInstance;
    }

    private VolleyHelper(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        // Start processing Request Queues
        mRequestQueue.start();
    }

    public Request addToRequestQueue(Request request) {
        mCurrentRequest = request;
        return mRequestQueue.add(request);
    }

    public Request addToRequestQueue(Request request, String tag) {
        request.setTag(tag);
        mCurrentRequest = request;
        return mRequestQueue.add(request);
    }

    public void cancelRequest(String tag) {
        mRequestQueue.cancelAll(tag);
    }

    public void doLastRequest() {
        if (mCurrentRequest != null) {
            mRequestQueue.add(mCurrentRequest);
            mCurrentRequest = null;
        }
    }
}
